from events.keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests, json


def get_picture(city, state):
    headers = {
        "Authorization": PEXELS_API_KEY,
    }
    url = "https://api.pexels.com/v1/search"
    params = {
        "query": str(city) + ", " + str(state),
    }
    res = requests.get(url, headers=headers, params=params)
    content = json.loads(res.content)
    picture_url = content["photos"][0]["src"]["original"]
    try:
        return {"picture_url" : picture_url}
    except:
        return {"picture_url" : None}


def get_weather(city, state):
    geourl = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q" : str(city) + "," + str(state) + ",840",
        "appid": OPEN_WEATHER_API_KEY,
    }
    res = requests.get(geourl, params=params)
    data = json.loads(res.content)
    lat = str(data[0]["lat"])
    lon = str(data[0]["lon"])

    url = "https://api.openweathermap.org/data/2.5/weather"
    params2 = {
        "lat, lon" : lat + "," + lon,
        "appid" : OPEN_WEATHER_API_KEY,
    }
    res2 = requests.get(url, params=params)
    weatherdata = json.loads(res2.content)
    temp = str(1.8*(weatherdata['main']['temp']-273) + 32)
    description = weatherdata['weather'][0]['description']
    return {
        "temperature" : temp,
        "description" : description,
    }